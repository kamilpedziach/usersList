import React, { useState } from "react";
import "./Item.css";
const Item = ({ user, clickedUsers }) => {
  const [checkbox, setCheckbox] = useState(false);

  const handleChangeCheckbox = (e) => {
    setCheckbox((prev) => !prev);
  };

  const handleData = (e) => {
    //wyświetlanie w konsoli klikniętych
    if (!checkbox) {
      const clickedUser = {
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
      };
      clickedUsers.current.push(clickedUser);
      console.log(clickedUsers.current);
    } else {
      //obsługa odkliknięcia
      clickedUsers.current = clickedUsers.current.filter(
        (u) => u.first_name !== user.first_name
      );
      console.log(clickedUsers.current);
    }
    setCheckbox((prev) => !prev);
  };

  return (
    <div
      className="item"
      onClick={handleData}
      style={checkbox ? { backgroundColor: "rgb(196, 235, 235)" } : {}}
    >
      <img src={user.avatar} alt="avatar" />
      <div className="desc">
        <span className="name">
          {user.first_name} {user.last_name}
        </span>
        <span className="email">{user.email}</span>
      </div>
      <input
        type="checkbox"
        className="checkbox"
        checked={checkbox}
        onChange={handleChangeCheckbox}
      />
    </div>
  );
};

export default Item;
