import React, { useEffect, useState, useRef } from "react";
import "./App.css";
import Item from "./components/Item/Item";
import { Search } from "@material-ui/icons";

const App = () => {
  const [users, setUsers] = useState([]);
  const searchData = useRef();
  const clickedUsers = useRef([]);
  const searchInput = useRef("");
  useEffect(() => {
    fetch(
      "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
    )
      .then((response) => response.json())
      .then((data) => {
        const sortedData = data.sort((a, b) => {
          if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) return -1;
          if (a.last_name.toLowerCase() > b.last_name.toLowerCase()) return 1;
          return 0;
        });
        setUsers(sortedData);
        searchData.current = sortedData;
      });
  }, []);

  const handleSearchData = (e) => {
    searchInput.current.value = e.target.value;
    const data = searchData.current.filter(
      (user) =>
        user.first_name.toLowerCase().includes(e.target.value.toLowerCase()) ||
        user.last_name.toLowerCase().includes(e.target.value.toLowerCase())
    );
    setUsers(data);
  };
  const userList = users.map((user) => (
    <Item user={user} key={user.id} clickedUsers={clickedUsers} />
  ));
  return (
    <div className="wrapper">
      <div className="topbar">
        <span>Contacts</span>
      </div>
      <div className="search">
        <Search className="icon" />
        <input type="text" onChange={handleSearchData} ref={searchInput} />
      </div>
      <div className="list">{userList}</div>
    </div>
  );
};

export default App;
